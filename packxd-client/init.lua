local libc_service = require 'libc_service'
local stream = require 'stream'
local system = require 'system'
local json = require 'protocol'
local pipe = require 'pipe'
local time = require 'time'
local unix = require 'unix'
local fs = require 'filesystem'

local arguments = system.arguments
table.remove(arguments, 2)
table.remove(arguments, 1)

if #arguments ~= 4 then
    print('Syntax:')
    print('<guest_name> <app_name> <profile> <app_bin>')
    system.exit(1)
end

local SPAWN_LINUX_APP_ARGS = {
    guest_name = arguments[1],
    app_name = arguments[2],
    profile = arguments[3],
    app_bin = arguments[4]
}

local SOCK_PATH = '/run/packxd.socket'

local runtime_path = fs.path.new(format(
    '/run/user/{user}/packxd/{app_name}/{profile}',
    {'user', select(2, system.getresuid()), nil},
    {'app_name', SPAWN_LINUX_APP_ARGS.app_name},
    {'profile', SPAWN_LINUX_APP_ARGS.profile}))

local preload_libc_path
do
    local pi, po = pipe.pair()
    po = po:release()
    pi = stream.scanner.new{ stream = pi }

    system.spawn{
        program = 'pkg-config',
        arguments = {'pkg-config', '--variable=libpath', 'emilua_preload_libc'},
        environment = system.environment,
        stdout = po,
    }
    po:close()

    preload_libc_path = tostring(pi:get_line())
end

local pin, pout = pipe.pair()
pout = pout:release()

local sock = unix.seqpacket.dial(SOCK_PATH)
local buf = byte_span.append(json.encode {
    request = 'spawn_linux_app',
    arguments = SPAWN_LINUX_APP_ARGS
})
sock:send_with_fds(buf, {pout})
pout:close()

local buf = byte_span.new(1024)
local nread, fds = sock:receive_with_fds(buf, 2)
local reply = json.decode(tostring(buf:first(nread)))
if reply.result ~= 'ok' then
    print('Failed: ' .. reply.error)
    system.exit(1)
end

pin = stream.scanner.new{ stream = pin }
spawn(function()
    while true do
        local line = pin:get_line()
        stream.write_all(
            system.out, byte_span.append('[xpra_server] ', line, '\n'))
    end
end):detach()

local ipc_control = unix.seqpacket.socket.new()
ipc_control:assign(fds[1])

local pulse = fds[2]
if pulse then
    local master, slave = libc_service.new()
    spawn(function()
        local devnull = fs.open(fs.path.new('/dev/null'), {'read_write'})
        local pulsedir = fs.path.new('/mnt/run/user/1000/pulse')
        local pid = pulsedir / 'pid'
        while true do
            master:receive()
            if master.function_ == 'mkdir' then
                if master:arguments() == pulsedir then
                    master:send(0)
                else
                    master:use_slave_credentials()
                end
            elseif master.function_ == 'open' then
                if master:arguments() == pid then
                    master:send_with_fds(-1, {devnull})
                else
                    master:use_slave_credentials()
                end
            else
                master:use_slave_credentials()
            end
        end
    end):detach()

    slave.open = [[
        local real_open, p, f, m = ...
        local res, errno, fd = real_open(p, f, m)
        if fd then
            return fd
        else
            return res, errno
        end
    ]]

    local env = system.environment
    env.LISTEN_PID = '\0pid'
    env.LISTEN_FDS = '1'
    env.LISTEN_FDNAMES = 'pipewire-pulse.socket'
    env.PULSE_RUNTIME_PATH = '/mnt/run/user/1000/pulse'
    env.LD_PRELOAD = preload_libc_path
    env.EMILUA_LIBC_SERVICE_FD = '4'
    system.spawn{
        program = 'pipewire-pulse',
        arguments = {'pipewire-pulse'},
        environment = env,
        stdout = 'share',
        stderr = 'share',
        extra_fds = {
            [3] = pulse,
            [4] = slave,
        },
        signal_on_gcreaper = 0,
        pdeathsig = system.signal.SIGKILL
    }
    pulse:close()
end

spawn(function()
    pcall(function()
        local buf = byte_span.new(1)
        ipc_control:receive(buf)
    end)
    system.exit(0)
end):detach()

local sigset = system.signal.set.new(
    system.signal.SIGTERM, system.signal.SIGINT)
spawn(function()
    sigset:wait()
    system.exit(0)
end):detach()

while true do
    local p = system.spawn{
        program = 'xpra',
        arguments = {
            'xpra',
            'attach',
            tostring(runtime_path / 'xpra/xpra'),
            '--opengl=no',
            '--cursors=no',
            '--remote-logging=no',
            '--session-name=' .. SPAWN_LINUX_APP_ARGS.app_name .. '/'
                .. SPAWN_LINUX_APP_ARGS.profile,
            '--title=' .. SPAWN_LINUX_APP_ARGS.app_name .. '/'
                .. SPAWN_LINUX_APP_ARGS.profile .. ' - @title@',
            '--mmap=' .. tostring(runtime_path / 'xpra/mmap/xpra')
        },
        stdout = 'share',
        stderr = 'share',
        environment = system.environment,
        pdeathsig = system.signal.SIGTERM
    }
    p:wait()
    if p.exit_code == 0 then
        system.exit(0)
    end
    if p.exit_signal then
        print('xpra-attach killed by signal', p.exit_signal)
        system.exit(0)
    end
    time.sleep(1)
end
