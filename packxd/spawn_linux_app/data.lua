GUEST_CODE = [[
    local system = require 'system'
    local inbox = require 'inbox'
    local unix = require 'unix'
    local time = require 'time'
    local fs = require 'filesystem'

    local ipc_channel = unix.seqpacket.socket.new()
    ipc_channel:assign(inbox:receive())
    inbox:close()

    -- 4096 = max number of BPF instructions
    -- 8 = bytes/instruction
    local bpf = byte_span.new(4096 * 8)
    do
        local nread = ipc_channel:receive(bpf)
        bpf = bpf:first(nread)
        if #bpf == 1 then
            bpf = nil
        end
    end

    local xpra_env = {{
        HOME = '/home/user',
        LOGNAME = 'user',
        USER = 'user',
        XDG_RUNTIME_DIR = '/run/user/1000'
    }}
    for k, v in pairs(system.environment) do
        xpra_env[k] = v
    end

    -- TODO: move some hardcoded settings to a conf file
    local p = system.spawn{{
        seccomp_set_mode_filter = bpf,
        program = 'xpra',
        arguments = {{
            'xpra',
            'start',
            '--daemon=no',
            '--mdns=no',
            '--dbus-launch=no',
            '--notifications=no',
            '--bind=/home/user/.xpra/xpra',
            '--speaker=no',
            '--use-display=no',
            '--xsettings=no',
            '--forward-xdg-open=no',
            '--terminate-children=yes',
            '--exit-with-children=yes',
            '--env=QT_QUICK_BACKEND=software',
            '--start-child={app_bin}',
            '--mmap=/home/user/.xpra/mmap/xpra'
        }},
        environment = xpra_env,
        stdout = 'share',
        stderr = 'share',
        working_directory = fs.path.new('/home/user')
    }}

    local waiter = spawn(function()
        pcall(function()
            local buf = byte_span.new(1)
            ipc_channel:receive(buf)
        end)
        if not p.pid then
            return
        end
        p:kill(system.signal.SIGTERM)
        print('SIGTERM sent')
        time.sleep(10)
        if not p.pid then
            return
        end
        print('SIGTERM timeout reached... sending SIGKILL')
        p:kill(system.signal.SIGKILL)
    end)

    p:wait()
    waiter:cancel()
    waiter:detach()
]]

INIT_SCRIPT = [[
    -- sync point #1
    C.read(arg, 1)
    C.write(arg, '.')

    -- sync point #2 as tmpfs will fail on mkdir()
    -- with EOVERFLOW if no UID/GID mapping exists
    -- https://bugzilla.kernel.org/show_bug.cgi?id=183461
    C.read(arg, 1)

    C.setresuid(0, 0, 0)
    C.setresgid(0, 0, 0)

    -- unshare propagation events
    C.mount(nil, '/', nil, C.MS_PRIVATE)

    -- acquire cgroup handle before we pivot_root
    local cgroup = C.open(
        '/sys/fs/cgroup{cgroup}/guests.slice/{app_name}.slice/' ..
        '{profile}.slice/user-{user}.slice/session.slice/.scope/cgroup.procs',
        C.O_WRONLY)

    -- forbid nested (user) namespaces
    C.write(C.open('/proc/sys/user/max_user_namespaces', C.O_WRONLY), '0')

    -- mount rootfs
    C.umask(0)
    C.mount(
        nil, '/mnt', 'tmpfs', bit.bor(C.MS_NOSUID, C.MS_NODEV, C.MS_RELATIME))
    C.mkdir('/mnt/proc', mode(7, 5, 5))
    C.mount(
        nil, '/mnt/proc', 'proc', bit.bor(C.MS_NOSUID, C.MS_NODEV, C.MS_NOEXEC),
        'hidepid=ptraceable,subset=pid')
    C.mkdir('/mnt/tmp', bit.bor(mode(7, 7, 7), C.S_ISVTX))
    C.mkdir('/mnt/run', mode(7, 5, 5))
    C.mkdir('/mnt/run/user', mode(7, 5, 5))
    C.mkdir('/mnt/run/user/1000', mode(7, 0, 0))
    C.chown('/mnt/run/user/1000', 1000, 1000)
    C.mkdir('/mnt/etc', mode(7, 5, 5))

    C.mkdir('/mnt/dev', mode(7, 5, 5))
    C.mkdir('/mnt/dev/pts', mode(7, 5, 5))
    C.mkdir('/mnt/dev/shm', bit.bor(mode(7, 7, 7), C.S_ISVTX))
    C.mkdir('/mnt/dev/mqueue', bit.bor(mode(7, 7, 7), C.S_ISVTX))
    C.symlink('/proc/self/fd', '/mnt/dev/fd')
    C.symlink('/dev/pts/ptmx', '/mnt/dev/ptmx')
    C.symlink('/proc/self/fd/0', '/mnt/dev/stdin')
    C.symlink('/proc/self/fd/1', '/mnt/dev/stdout')
    C.symlink('/proc/self/fd/2', '/mnt/dev/stderr')
    C.mknod('/mnt/dev/full', mode(6, 6, 6), 0)
    C.mount('/dev/full', '/mnt/dev/full', nil, C.MS_BIND)
    C.mknod('/mnt/dev/null', mode(6, 6, 6), 0)
    C.mount('/dev/null', '/mnt/dev/null', nil, C.MS_BIND)
    C.mknod('/mnt/dev/random', mode(6, 6, 6), 0)
    C.mount('/dev/random', '/mnt/dev/random', nil, C.MS_BIND)
    C.mknod('/mnt/dev/tty', mode(6, 6, 6), 0)
    C.mount('/dev/tty', '/mnt/dev/tty', nil, C.MS_BIND)
    C.mknod('/mnt/dev/urandom', mode(6, 6, 6), 0)
    C.mount('/dev/urandom', '/mnt/dev/urandom', nil, C.MS_BIND)
    C.mknod('/mnt/dev/zero', mode(6, 6, 6), 0)
    C.mount('/dev/zero', '/mnt/dev/zero', nil, C.MS_BIND)
    C.mount(nil, '/mnt/dev/pts', 'devpts', bit.bor(C.MS_NOSUID, C.MS_NOEXEC),
            'newinstance,gid=1000,mode=620,ptmxmode=0666')
    C.mount(nil, '/mnt/dev/mqueue', 'mqueue',
            bit.bor(C.MS_NOSUID, C.MS_NODEV, C.MS_NOEXEC))

    local passwd = C.open(
        '/mnt/etc/passwd', bit.bor(C.O_WRONLY, C.O_CREAT), mode(6, 4, 4))
    write_all(passwd, 'user:x:1000:1000::/home/user:/bin/sh\n')

    local machineid = C.open(
        '/mnt/etc/machine-id', bit.bor(C.O_WRONLY, C.O_CREAT), mode(6, 4, 4))
    write_all(machineid, '00000000000000000000000000000000\n')

    local resolvconf = C.open(
        '/mnt/etc/resolv.conf', bit.bor(C.O_WRONLY, C.O_CREAT), mode(6, 4, 4))
    write_all(resolvconf, {resolvconf:?})

    C.mkdir('/mnt/home', mode(7, 5, 5))
    C.mkdir('/mnt/home/user', mode(7, 5, 5))
    C.mount(
        '/var/lib/packxd/{user}/{app_name}/{profile}',
        '/mnt/home/user',
        nil,
        C.MS_BIND)
    C.mount(
        nil, '/mnt/home/user', nil,
        bit.bor(C.MS_REMOUNT, C.MS_BIND, C.MS_NOSUID, C.MS_NODEV))

    {root_bindmounts}

    {root_symlinks}

    C.mount(
        '/run/user/{user}/packxd/{app_name}/{profile}/xpra',
        '/mnt/home/user/.xpra',
        nil,
        C.MS_BIND)

    C.mkdir('/mnt/run/user/1000/pulse', mode(7, 0, 0))
    C.chown('/mnt/run/user/1000/pulse', 1000, 1000)
    local pulse = C.socket(C.AF_UNIX, C.SOCK_STREAM, 0)
    bind_unix(pulse, '/mnt/run/user/1000/pulse/native')
    C.chmod('/mnt/run/user/1000/pulse/native', mode(6, 6, 6))
    C.listen(pulse, C.SOMAXCONN)
    send_with_fd(arg, '.', pulse)

    -- pivot root
    C.mkdir('/mnt/mnt', mode(7, 5, 5))
    C.chdir('/mnt')
    C.pivot_root('.', '/mnt/mnt')
    C.chroot('.')
    C.umount2('/mnt', C.MNT_DETACH)

    C.sethostname('{hostname}')
    C.setdomainname('{hostname}')

    -- Move to cgroup.
    --
    -- It must happen after host side is done. The cgroup is under user-control
    -- and he could have had the cgroup frozen (then we're possibly freezing the
    -- process here). From here on now, host (the root daemon) can no longer
    -- synchronize against us, or it could expose itself to DoS attacks from the
    -- user.
    C.write(cgroup, '0')
    C.unshare(C.CLONE_NEWCGROUP)

    C.umask(mode(0, 2, 2))

    -- drop all root privileges
    C.setresgid(1000, 1000, 1000)
    C.setgroups({{1000}})
    C.setresuid(1000, 1000, 1000)
    set_no_new_privs()

    C.setsid()
]]
