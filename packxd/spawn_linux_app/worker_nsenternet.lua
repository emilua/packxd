local stream = require 'stream'
local system = require 'system'
local inbox = require 'inbox'
local file = require 'file'
local json = require 'protocol'
local unix = require 'unix'
local fs = require 'filesystem'

local data = require '../data'

local msg = inbox:receive()
local host = msg.host
local sguest = msg.sguest
local guest_name = msg.guest_name
local expansions = {}
local uid

for k, v in pairs(inbox:receive()) do
    expansions[#expansions + 1] = {k, v}
    if k == 'user' then
        uid = v
    end
end

local conf
do
    conf = file.stream.new()
    conf:open(fs.path.new('/etc/packxd.json'), {'read_only'})
    local buf = byte_span.new(conf.size)
    stream.read_all(conf, buf)
    conf = json.decode(tostring(buf)).guests[guest_name]
end

local cgroup
do
    local f = file.stream.new()
    f:open(fs.path.new('/proc/self/cgroup'), {'read_only'})
    f = stream.scanner.new{ stream = f, field_separator = ':' }
    cgroup = tostring(f:get_line()[3])

    local cgrouppath = fs.path.new('/sys/fs/cgroup') /
        fs.path.new(cgroup).relative_path /
        fs.path.new(format(
            'guests.slice/{app_name}.slice/{profile}.slice/user-{user}.slice',
            unpack(expansions)))
    fs.create_directories(cgrouppath)
    fs.chown(cgrouppath, uid, -1)
    system.setresuid(-1, uid, -1)
    fs.create_directories(cgrouppath / 'session.slice/.scope')
    system.setresuid(-1, 0, -1)
end

local root_bindmounts = ''
for _, v in ipairs(conf.root_bindmounts) do
    if fs.is_directory(fs.path.new(format('{rootfs}{0}', v, unpack(expansions)))) then
        root_bindmounts = root_bindmounts .. format([[

            C.mkdir('/mnt{}', mode(7, 5, 5))

        ]], v)
    else
        root_bindmounts = root_bindmounts .. format([[

            C.mknod('/mnt{}', mode(7, 5, 5), 0)

        ]], v)
    end

    root_bindmounts = root_bindmounts .. format([[

        local rbmount = C.open_tree(
            C.AT_FDCWD, '{rootfs}{tgt}',
            bit.bor(C.OPEN_TREE_CLONE, C.AT_RECURSIVE))
        C.mount_setattr(
            rbmount, '', bit.bor(C.AT_EMPTY_PATH, C.AT_RECURSIVE),
            {{
                attr_set = bit.bor(
                    C.MOUNT_ATTR_RDONLY, C.MOUNT_ATTR_NOSUID,
                    C.MOUNT_ATTR_NODEV)
            }})
        C.move_mount(rbmount, '', C.AT_FDCWD, '/mnt{tgt}', C.MOVE_MOUNT_F_EMPTY_PATH)

    ]], {'tgt', v}, unpack(expansions))
end

local root_symlinks = ''
for linkpath, target in pairs(conf.root_symlinks) do
    root_symlinks = root_symlinks ..
        format('C.symlink("{}", "/mnt{}")\n', target, linkpath)
end

local guest_channel = spawn_vm{
    module = fs.path.new('/app.lua'),
    subprocess = {
        newns_user = true,
        newns_mount = true,
        newns_pid = true,
        newns_uts = true,
        newns_ipc = true,
        init = {
            arg = sguest,
            script = format(
                data.INIT_SCRIPT,
                {'root_bindmounts', root_bindmounts},
                {'root_symlinks', root_symlinks},
                {'resolvconf', conf['resolv.conf']},
                {'cgroup', cgroup},
                unpack(expansions))
        },
        source_tree_cache = {
            ['app.lua'] = format(data.GUEST_CODE, unpack(expansions))
        },
        stdout = 'share',
        stderr = 'share',
        environment = conf.environment
    }
}

local shost2, sguest2 = unix.seqpacket.socket.pair()
shost2, sguest2 = shost2:release(), sguest2:release()

guest_channel:send(sguest2)
sguest2:close()

local child_pid = guest_channel.child_pid
guest_channel:detach()

host:send{
    pid = child_pid,
    ipc_control = shost2
}
