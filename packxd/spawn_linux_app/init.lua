local generic_error = require 'generic_error'
local stream = require 'stream'
local system = require 'system'
local inbox = require 'inbox'
local mutex = require 'mutex'
local regex = require 'regex'
local file = require 'file'
local json = require 'protocol'
local unix = require 'unix'
local fs = require 'filesystem'
local select, unpack, format = select, unpack, format

local IGNORED_BUF = byte_span.new(1)

local NETMASKREGEX = regex.new{
    pattern = '^[0-9.]+(/[0-9]+)$',
    grammar = 'extended',
    optimize = true
}

local WORKER_CREATE_USERRUNDIRS = [[
local system = require 'system'
local inbox = require 'inbox'
local fs = require 'filesystem'

system.setresgid(-1, {gid}, -1)
system.setresuid(-1, {uid}, -1)

local runtime_path = fs.path.new({runtime_path:?})
fs.create_directories(runtime_path / 'xpra/mmap')
fs.chmod(runtime_path / 'xpra', fs.mode(7, 0, 0))
fs.chmod(runtime_path / 'xpra/mmap', fs.mode(7, 0, 0))
fs.create_directories(runtime_path / 'pulse')

local master = inbox:receive()
master:send('done')
]]

local inbox_mtx = mutex.new()
local veth_mtx = mutex.new()

function handler(conf, client, args, fds)
    local uid = client:get_option('remote_credentials').uid
    local gid = client:get_option('remote_credentials').groups[1]
    local guest_name = args.guest_name
    local expansions = {
        {'user', uid},
        {'guest_name', guest_name},
        {'app_name', args.app_name},
        {'app_bin', args.app_bin},
        {'profile', args.profile},
        {'rootfs', conf.guests[guest_name].rootfs},
        {'hostname', conf.guests[guest_name].hostname}
    }
    local pout = fds[1]

    local netns_name = 'packxd-' .. guest_name .. '-0'
    local p = system.spawn{
        program = 'ip',
        arguments = {'ip', 'netns', 'add', netns_name},
    }
    p:wait()
    if p.exit_code == 0 then
        local guest_address = conf.guests[guest_name].veth.ipv4.address
        local gateway_address = conf.guests[guest_name].veth.ipv4.gateway
        local netmask = regex.match(NETMASKREGEX, guest_address)

        system.spawn{
            program = 'ip',
            arguments = {
                'ip', 'netns', 'exec', netns_name,
                'ip', 'link', 'set', 'dev', 'lo', 'up'}
        }:wait()

        local veth_name0 = conf.guests[guest_name].veth.ifname

        scope(function()
            veth_mtx:lock()
            scope_cleanup_push(function() veth_mtx:unlock() end)

            system.spawn{
                program = 'ip',
                arguments = {
                    'ip', 'link', 'add',
                    veth_name0, 'type', 'veth', 'peer', 'name', '_packxd'}
            }:wait()

            system.spawn{
                program = 'ip',
                arguments = {
                    'ip', 'link', 'set', '_packxd', 'netns', netns_name}
            }:wait()
        end)

        system.spawn{
            program = 'ip',
            arguments = {
                'ip', 'netns', 'exec', netns_name,
                'ip', 'link', 'set', 'dev', '_packxd', 'name', 'eth0'}
        }:wait()

        system.spawn{
            program = 'ip',
            arguments = {
                'ip', 'netns', 'exec', netns_name,
                'ip', 'address', 'add', guest_address, 'dev', 'eth0'}
        }:wait()

        system.spawn{
            program = 'ip',
            arguments = {
                'ip', 'netns', 'exec', netns_name,
                'ip', 'link', 'set', 'dev', 'eth0', 'up'}
        }:wait()

        system.spawn{
            program = 'ip',
            arguments = {
                'ip', 'netns', 'exec', netns_name,
                'ip', 'route', 'add', 'default', 'via', gateway_address}
        }:wait()

        system.spawn{
            program = 'ip',
            arguments = {
                'ip', 'address', 'add',
                gateway_address .. netmask, 'dev', veth_name0}
        }:wait()

        system.spawn{
            program = 'ip',
            arguments = {'ip', 'link', 'set', 'dev', veth_name0, 'up'}
        }:wait()
    end

    local netns = file.stream.new()
    netns:open(
        fs.path.new('/var/run/netns/' .. netns_name),
        {'read_only'})
    netns = netns:release()
    scope_cleanup_push(function() netns:close() end)

    local guest_channel = spawn_vm{
        module = fs.path.new('/a.lua'),
        subprocess = {
            source_tree_cache = {
                ['a.lua'] = format(
                    WORKER_CREATE_USERRUNDIRS,
                    {'uid', uid},
                    {'gid', gid},
                    {
                        'runtime_path',
                        format(
                            '/run/user/{user}/packxd/{app_name}/{profile}',
                            unpack(expansions))
                    })
            },
            stdout = 'share',
            stderr = 'share'
        }
    }

    scope(function()
        inbox_mtx:lock()
        scope_cleanup_push(function() inbox_mtx:unlock() end)

        -- TODO: use a pipe to get rid of the mutex
        guest_channel:send(inbox)
        inbox:receive()
    end)

    local datadir = fs.path.new(format(
        '/var/lib/packxd/{user}/{app_name}/{profile}',
        unpack(expansions)))

    fs.create_directories(datadir.parent_path)
    local ok, e = pcall(fs.mkdir, datadir, fs.mode(7, 0, 0))
    if not ok and e:togeneric() ~= generic_error.EEXIST then
        error(e)
    end

    fs.create_directories(datadir / '.xpra')
    fs.chown(datadir, uid, gid)
    fs.chown(datadir / '.xpra', uid, gid)

    local shost, sguest = unix.seqpacket.socket.pair()
    sguest = sguest:release()

    local guest_channel = spawn_vm{
        module = './worker_nsenternet',
        subprocess = {
            init = {
                arg = netns,
                script = 'C.setns(arg, C.CLONE_NEWNET)'
            },
            environment = system.environment,
            stdout = pout,
            stderr = pout
        }
    }
    if pout then
        pout:close()
    end

    local child_pid
    local ipc_control = unix.seqpacket.socket.new()

    scope(function()
        inbox_mtx:lock()
        scope_cleanup_push(function() inbox_mtx:unlock() end)

        guest_channel:send{
            host = inbox,
            sguest = sguest,
            guest_name = guest_name
        }
        sguest:close()

        do
            local expansions2 = {}
            for _, p in ipairs(expansions) do
                expansions2[p[1]] = p[2]
            end
            guest_channel:send(expansions2)
        end

        local reply = inbox:receive()
        child_pid = reply.pid
        ipc_control:assign(reply.ipc_control)
    end)

    guest_channel:close()

    local uidmap = file.stream.new()
    uidmap:open(
        fs.path.new(format('/proc/{}/uid_map', child_pid)),
        {'write_only'})
    scope_cleanup_push(function() uidmap:close() end)

    local setgroups = file.stream.new()
    setgroups:open(
        fs.path.new(format('/proc/{}/setgroups', child_pid)),
        {'write_only'})
    scope_cleanup_push(function() setgroups:close() end)

    local gidmap = file.stream.new()
    gidmap:open(
        fs.path.new(format('/proc/{}/gid_map', child_pid)),
        {'write_only'})
    scope_cleanup_push(function() gidmap:close() end)

    -- Sync point #1
    --
    -- This sync point makes sure child_pid still is valid and we haven't opened
    -- the wrong proc files. We only write to the files after that.
    shost:send(IGNORED_BUF)
    shost:receive(IGNORED_BUF)

    uidmap:write_some(byte_span.append(format('0 0 1\n1000 {} 1\n', uid)))
    setgroups:write_some(byte_span.append('allow'))
    gidmap:write_some(byte_span.append(format('0 0 1\n1000 {} 1\n', gid)))

    -- sync point #2
    shost:send(IGNORED_BUF)

    local pulse = select(2, shost:receive_with_fds(IGNORED_BUF, 1))[1]
    shost:close()

    if conf.guests[guest_name].seccomp then
        ipc_control:send(conf.guests[guest_name].seccomp)
    else
        ipc_control:send(byte_span.append('.'))
    end

    client:send_with_fds(
        byte_span.append(json.encode {result = 'ok'}),
        { ipc_control:release(), pulse })
    pulse:close()
    ipc_control:close()
end
