local stream = require 'stream'
local system = require 'system'
local kafel = require 'kafel'
local unix = require 'unix'
local json = require 'protocol'
local file = require 'file'
local time = require 'time'
local fs = require 'filesystem'

local SOCK_PATH = fs.path.new('/run/packxd.socket')

local notify_socket
do
    local env = system.environment.NOTIFY_SOCKET
    if env then
        local c = string.sub(env, 1, 1)
        if c ~= '/' and c ~= '@' then
            stream.write_all(system.err, '<3>NOTIFY_SOCKET not supported\n')
            system.exit(1)
        end

        notify_socket = unix.datagram.dial(env)
    end
end

local HANDLERS = {
    spawn_linux_app = require('./spawn_linux_app').handler
}

local conf

do
    local function load_conf()
        scope(function()
            local f = file.stream.new()
            scope_cleanup_push(function() f:close() end)
            f:open(fs.path.new('/etc/packxd.json'), {'read_only'})
            local buf = byte_span.new(f.size)
            stream.read_all(f, buf)
            local rawconf = json.decode(tostring(buf))
            for _, guest in pairs(rawconf.guests) do
                if guest.seccomp then
                    table.insert(guest.seccomp, 1, kafel.default)
                    guest.seccomp = kafel.compile(
                        table.concat(guest.seccomp, '\n'))
                end
            end
            conf = rawconf
        end)
    end
    load_conf()

    local sigset = system.signal.set.new(system.signal.SIGHUP)
    spawn(function()
        while true do
            sigset:wait()
            if notify_socket then
                notify_socket:send(byte_span.append(format(
                    'RELOADING=1\nMONOTONIC_USEC={:.0f}\n',
                    time.steady_clock.now().seconds_since_epoch * 1000000)))
            else
                stream.write_all(
                    system.err, '<5>Loading config file (SIGHUP received)\n')
            end
            if pcall(load_conf) then
                if not notify_socket then
                    stream.write_all(system.err, '<5>New config file loaded\n')
                end
            else
                stream.write_all(system.err, '<3>Invalid config file\n')
            end
            if notify_socket then
                notify_socket:send(byte_span.append('READY=1\n'))
            end
        end
    end):detach()
end

local acceptor
if system.environment.LISTEN_PID == tostring(system.getpid()) then
    acceptor = unix.seqpacket.acceptor.new()
    acceptor:assign(system.get_lowfd(3))
else
    fs.remove(SOCK_PATH)
    acceptor = unix.seqpacket.listen(tostring(SOCK_PATH), fs.mode(6, 6, 6))
    scope_cleanup_push(function() fs.remove(SOCK_PATH) end)
end

do
    local sigset = system.signal.set.new(
        system.signal.SIGINT, system.signal.SIGTERM)
    spawn(function()
        local signo = sigset:wait()
        stream.write_all(
            system.err, format('<5>Signal {} received. Exiting...\n', signo))
        system.exit(0)
    end):detach()
end

if notify_socket then
    notify_socket:send(byte_span.append('READY=1\n'))
end

local function handle_client(client)
    scope_cleanup_push(function() client:close() end)
    local client_uid = client:get_option('remote_credentials').uid
    local user_permitted = false
    for _, uid in ipairs(conf.permit_users) do
        if client_uid == uid then
            user_permitted = true
            break
        end
    end
    if not user_permitted then
        client:send(byte_span.append(json.encode {
            error = 'User not permitted'
        }))
        return
    end
    local buf = byte_span.new(1024)
    while true do
        local nread, fds = client:receive_with_fds(buf, 1)
        scope(function()
            scope_cleanup_push(function()
                for _, fd in ipairs(fds) do fd:close() end
            end)
            local req = json.decode(tostring(buf:first(nread)))
            HANDLERS[req.request](conf, client, req.arguments, fds)
        end)
    end
end

while true do
    local client = acceptor:accept()
    spawn(function() handle_client(client) end):detach()
end
